package db

import (
	"database/sql"
	"sync"

	_ "github.com/lib/pq"
	"github.com/sirupsen/logrus"
	"gitlab.com/multitech/go_server/config"
	"gitlab.com/multitech/go_server/oopsies"
)

var Driver *sql.DB
var once sync.Once

func init() {
	// get database URL
	dbURL := config.Vars.DatabaseURL

	// open the database
	pgDriver, err := sql.Open("postgres", dbURL)
	if err != nil {
		logrus.Fatal(oopsies.DBOpen, err.Error())
	}

	// must do this in separate line
	// doing above with := will create a new
	// local variable called Driver
	Driver = pgDriver
}
