package main

import (
	"database/sql"

	"log"
	"os"

	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
	"github.com/sirupsen/logrus"
	"gitlab.com/rakesh.rakshit/my-go-project/answers"
	"gitlab.com/rakesh.rakshit/my-go-project/questions"
	"gitlab.com/rakesh.rakshit/my-go-project/users"
)

func main() {

	var err error

	db, err := sql.Open("postgres", os.Getenv("DATABASE_URL"))
	if err != nil {
		log.Fatalf("Error opening database: %q", err)
	}

	logrus.Info(db)

	r := gin.Default()
	r.Use(gin.Logger())

	r.GET("/", func(c *gin.Context) {
		c.String(200, "welcome")
	})

	r.GET("/questions", questions.GetQuestions)
	r.POST("/questions", questions.PostQuestions)

	r.GET("/answers", answers.GetAnswers)
	r.POST("/answers", answers.PostAnswers)

	r.PUT("/users", users.UpdateUserDetails)
	r.GET("/users", users.GetUserDetails)

	r.POST("/following", users.AddFollowing)
	r.GET("/following", users.GetFollowing)
	r.DELETE("/following", users.DeleteFollowing)

	r.Run() // listen and serve on 0.0.0.0:8080
}
