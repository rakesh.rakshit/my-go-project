package configloader

import (
	"encoding/json"
	"os"

	"github.com/sirupsen/logrus"
)

// Load reads the named JSON configuration file. This
// function fails if the file is not found or cannot be
// parsed. The loaded configuration is available in config.Vars
func Load(filePath string, configVar interface{}) {
	//filename is the path to the json config file
	file, err := os.Open(filePath)
	if err != nil {
		logrus.Fatal("Cound not open config file", filePath, err)
	}

	decoder := json.NewDecoder(file)
	err = decoder.Decode(configVar)
	if err != nil {
		logrus.Fatal("Could not parse config file", filePath, err)
	}
}
