package config

import (
	"flag"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/wercker/journalhook"
	"gitlab.com/multitech/go_server/configloader"
)

const (

	//DB database name
	DB = "forumdb"
)

type Config struct {
	Name                  string
	Port                  string
	DatabaseURL           string
	NSQDAddr              string
	NSQDPort              string
	NSQDTopic             string	
	DisableJournalLogging bool
}

// Vars contains the loaded configuration
var Vars Config
var ConfigFilePath string

// ReadCommandLineArgs defines and reads arguments passed to command line flags.
// conf: path to config file. Recommended to be absolute path.
func ReadCommandLineArgs() {
	flag.StringVar(&ConfigFilePath, "conf", "", "Non-empty absolute path to config file")

	if !flag.Parsed() {
		flag.Parse()
	}
}

func GetDBURL(dbname string) string {
	return "postgres://postgres@localhost:5432/" + dbname + "?sslmode=disable"
}

func init() {
	ReadCommandLineArgs()

	if len(ConfigFilePath) == 0 {
		logrus.Fatal("No configuration file specified")
	}

	// setup logging
	logrus.SetLevel(logrus.DebugLevel)

	logrus.Info("Reading server configuration from ", ConfigFilePath)
	configloader.Load(ConfigFilePath, &Vars)

	dbname := "unknown"

	switch Vars.Name {
	case "local", "test":
		Vars.DatabaseURL = GetDBURL(DB)
		dbname = DB
	default:
		logrus.Fatal("Unsupported configuration name: ", Vars.Name)
	}

	logrus.Info("Connected to database: ", dbname)

	if !Vars.DisableJournalLogging {
		journalhook.Enable() // improved logging to journald
	}
}
