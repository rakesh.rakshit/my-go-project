CREATE TABLE public.questions (
    userid bigint ,
    quesid bigserial NOT NULL,
    question text,
    semnum INTEGER,
    subjectname VARCHAR(20),
    stream VARCHAR(20),
    PRIMARY KEY (quesid)
);