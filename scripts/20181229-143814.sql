CREATE TABLE public.authidentifiers (
    userid bigserial NOT NULL,
    email varchar(320) UNIQUE CHECK (char_length(email) > 0),
    phone varchar UNIQUE CHECK (char_length(phone) > 0),
    PRIMARY KEY (userid)
);